package com.example.meme_app;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class Activity_Meme extends AppCompatActivity{
    private static ImageView imageview;
    private static TextView textview;
    private static ImageButton Button_Home;
    private static ImageButton Button_Refresh_Text;
    private static ImageButton Button_T_Favoutites;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "text";

    private static final String TAG = Activity_Meme.class.getName();

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonclick_back();
        buttonclick_favourites();
        buttonclick_reload_text();
    }

    public void buttonclick_reload_text(){
        textview = (TextView) findViewById(R.id.Text_Meme);
        Button_T_Favoutites = (ImageButton)findViewById(R.id.button_t_favourites);
        Button_Refresh_Text = (ImageButton)findViewById(R.id.button_refresh_text);
        OkHttpClient client = new OkHttpClient();
        Spinner category_spinner = (Spinner) findViewById(R.id.category_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.category_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(adapter);

        Button_Refresh_Text.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String callback = (String) category_spinner.getSelectedItem().toString();
                        String url = "https://api.chucknorris.io/jokes/random?category="+callback;
                        Request request = new Request.Builder()
                                .url(url)
                                .get()
                                .build();
                        Button_T_Favoutites.setImageResource(R.drawable.heart_nfill);
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                                e.printStackTrace();
                                Log.d(TAG, "onClick_Text");
                            }

                            @Override
                            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    String rawResponse = response.body().string();
                                    String textInfo = getTextInformationJoke(rawResponse);
                                    Log.d(TAG, "Response" + textInfo);
                                    runOnUiThread(()->textview.setText(textInfo));
                                }
                                response.close();
                            }
                        });
                    }
                }
        );
    }
    /*public void save(View view){
        Spinner category_Spinner = (Spinner)findViewById(R.id.spinner_category);
        category_Spinner.getSelectedItem().toString();
    }*/

    private String getTextInformationJoke(String rawResponse) {
        String results = "";
        try {
            JSONObject responseObject = new JSONObject(rawResponse);
            results =responseObject.getString("value");
        } catch (JSONException e) {
            Log.e(TAG, "Could not read json data!");
        }
        return results;
    }


    public void buttonclick_back(){
        Button_Home = (ImageButton)findViewById(R.id.button_home);
        Button_Home.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Activity_Meme.this, Activity_Favoutites.class));
                    }
                }
        );
    }
    public void buttonclick_favourites(){
        Button_T_Favoutites = (ImageButton)findViewById(R.id.button_t_favourites);
        Button_T_Favoutites.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Button_T_Favoutites.setImageResource(R.drawable.heart_fill);
                        //function to add to favourites list
                        saveData();
                    }
                }
        );
    }

    public void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        for(int i =0;i<sharedPreferences.getAll().size();i++){
            if (textview.getText().toString() == sharedPreferences.getString(""+i,"")){
                Button_T_Favoutites.setImageResource(R.drawable.heart_fill);
                Toast.makeText(this, "Meme already Saved", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if(!sharedPreferences.getString("29", "").equals("")){
            Toast.makeText(this, "Es können nur maximal 30 Texte gespeichert werden", Toast.LENGTH_SHORT).show();
            return;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        for(int i = 0; i < 30; i++){
            if(sharedPreferences.getString(""+i, null) == null){
                editor.putString(""+i,textview.getText().toString());
                break;
            }
        }
        editor.apply();
        Toast.makeText(this, "Meme Saved", Toast.LENGTH_SHORT).show();
    }
}