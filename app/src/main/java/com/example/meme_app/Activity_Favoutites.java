package com.example.meme_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.meme_app.Activity_Meme.SHARED_PREFS;
import static com.example.meme_app.Activity_Meme.TEXT;

public class Activity_Favoutites extends AppCompatActivity implements ExampleItemAdapter.ExampleItemAdapterCallback {
    private static Button Button_TO_Meme;
    private String joke = "";
    private TextView SavedMeme;
    private ArrayList exampleList =  new ArrayList<>();
    private ExampleItemAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__favoutites);
        buttonclick_TO_Meme();
        assignRecyclerview();

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
        if(adapter != null) {
            adapter.setDataset(exampleList);
            adapter.notifyDataSetChanged();
        }
    }

    public void buttonclick_TO_Meme(){
        Button_TO_Meme = (Button)findViewById(R.id.buttonToNew);
        Button_TO_Meme.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Activity_Favoutites.this, Activity_Meme.class));
                    }

                }

        );
    }

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        for(int i =0;i<sharedPreferences.getAll().size();i++){
            Log.d(Activity_Favoutites.class.getName(), i+": "+sharedPreferences.getString(""+i, ""));
            exampleList.add(new exampleitem(R.drawable.ic_android,sharedPreferences.getString(""+i,"")));
        }



    }


    public void assignRecyclerview(){
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        if(recyclerView != null){
            adapter = new ExampleItemAdapter(new ArrayList<exampleitem>(),this);
            recyclerView.setAdapter(adapter);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
        }

    }

    @Override
    public void removeItem(int index) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(""+index);
        for(int i = index; i < sharedPreferences.getAll().size(); i++){
            editor.putString(""+i, sharedPreferences.getString(""+(i+1), null));
        }
        editor.remove(""+(sharedPreferences.getAll().size()-1));
        editor.apply();
    }
}