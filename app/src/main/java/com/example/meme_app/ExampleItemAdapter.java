package com.example.meme_app;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ExampleItemAdapter extends RecyclerView.Adapter<ExampleItemAdapter.ViewHolder> {

        private List<exampleitem> mDataset;
        private Context context;
        private ExampleItemAdapterCallback exampleItemAdapterCallback;

        public interface ExampleItemAdapterCallback{
            void removeItem(int index);


        }

        /**
         * This ViewHolder is assigning the objects from row_client_playlist.xml to the global view-variables
         * @see RecyclerView.ViewHolder
         * @see TextView
         */
        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView memeTextView;
            private ImageView icon;
            private Button loeschButton;
            /**
             * Constructor
             * @param itemView parent view from row_client_playlist.xml
             */
            public ViewHolder(View itemView) {
                super(itemView);
                memeTextView = (TextView) itemView.findViewById(R.id.memeText);
                icon = itemView.findViewById(R.id.icon);
                loeschButton = itemView.findViewById(R.id.loeschButton);
            }
        }


        public ExampleItemAdapter(List<exampleitem> liste, ExampleItemAdapterCallback exampleItemAdapterCallback) {
            this.mDataset = liste;
            this.exampleItemAdapterCallback = exampleItemAdapterCallback;
        }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.favourites_row, parent, false));

    }

    @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            TextView meme = holder.memeTextView;
                if(meme != null){
                    meme.setText(mDataset.get(position).getmText1());
                }
            ImageView icon = holder.icon;
            if(icon != null){
                icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_android));
             }
            Button loeschButton = holder.loeschButton;
            if(loeschButton != null){
                loeschButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDataset.remove(position);
                        notifyDataSetChanged();
                        exampleItemAdapterCallback.removeItem(position);

                    }
                });
            }
        }

        @Override
        public int getItemCount() {

            return mDataset.size(); }


        public void setDataset(List<exampleitem> mDataset) {
            this.mDataset = mDataset;

        }
    }

